/*Creates the database*/
CREATE DATABASE fantasyhockey;

/*use the new database*/
USE fantasyhockey;

--
-- Database tables from Fantasy hockey
--

CREATE TABLE IF NOT EXISTS `UserAccess` (
  `user_access_id` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for UserAccess',
  `description` varchar(100) NOT NULL COMMENT 'describes accesss permissions for that user'
  )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
  INSERT INTO `UserAccess` (`user_access_id`, `description`) VALUES
(1, 'Basic access'),
(2, 'Admin access');
  /*Table structure for Users*/
  CREATE TABLE IF NOT EXISTS `Users` (
	`user_id` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for Users',
	`user_name` varchar(25)  NOT NULL COMMENT 'Name of the user',
	`user_password` varchar(32)  NOT NULL COMMENT 'user password',
	`user_email` varchar(25) NOT NULL COMMENT 'users email',
	`user_access` int(3) NOT NULL COMMENT 'sets users access level',
	 FOREIGN KEY (user_access) REFERENCES UserAccess(user_access_id)
  )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
  
  /*Dumping data for table Users*/
 INSERT INTO `Users` (`user_id`,`user_name`,`user_password`,`user_email`,`user_access`) VALUES
(1, 'admin', md5('123'),'admin@admin.com',2);
/*Table structure for table leagues*/

CREATE TABLE IF NOT EXISTS `Leagues` (
  `league_id` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for leagues',
  `user_id` int(3) NOT NULL COMMENT 'user_id as foreign key',
  `league_name` varchar(100) DEFAULT NULL COMMENT 'Name of the league',
  FOREIGN KEY (user_id) REFERENCES Users(user_id)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Dumping data for table Leagues*/
INSERT INTO `Leagues` (`league_id`, `user_id`, `league_name`) VALUES
(1, 1, 'The League');
 
 /*Table structure for Teams*/
CREATE TABLE IF NOT EXISTS `Teams` (
  `team_id` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for teams',
  `league_id` int(3) NOT NULL COMMENT 'league_id as foreign key',
  `team_owner` int(3)  NOT NULL COMMENT 'references the user who owns the team',
  `team_name` varchar(25) NOT NULL COMMENT 'name of the team',
  `team_wins` int(3) NULL DEFAULT 0 COMMENT 'keeps track of wins',
  FOREIGN KEY (league_id) REFERENCES Leagues(league_id),
  FOREIGN KEY (team_owner) REFERENCES Users(user_id)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
  
  /*Table structure for Players*/
CREATE TABLE IF NOT EXISTS `Players` (
	`player_id` int(4) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for players',
	`first_name` varchar(50)  NOT NULL COMMENT 'Name of the player',
	`last_name` varchar(50)  NOT NULL COMMENT 'Name of the player',
	`points` int(4) NOT NULL COMMENT 'number of points player has accumulated',
	`goals` int(4) NOT NULL COMMENT 'number of goals player has accumulated',
	`team_id` int(3) NULL COMMENT 'foriegn key from team',
	FOREIGN KEY (team_id) REFERENCES Teams(team_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

 /*Table structure for Trades*/
CREATE TABLE IF NOT EXISTS `trades` (
	`trade_id` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for trades',
	`team_one` int(3) NOT NULL COMMENT 'teamone id',
	`team_two` int(3) NOT NULL COMMENT 'teamone id',
	`team_one_player` int(4) NOT NULL COMMENT 'team one player to be traded',
	`team_two_player` int(4) NOT NULL COMMENT 'team two player to be traded',
	`approved` boolean NOT NULL COMMENT 'second player has accepted trade',
	FOREIGN KEY (team_one) REFERENCES Teams(team_id),
	FOREIGN KEY (team_two) REFERENCES Teams(team_id),
	FOREIGN KEY (team_one_player) REFERENCES Players(player_id),
	FOREIGN KEY (team_two_player) REFERENCES Players(player_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for Matches*/
CREATE TABLE IF NOT EXISTS `Matches` (
	`match_id` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for matches',
	`team_one` int(3) NOT NULL COMMENT 'teamone id',
	`team_two` int(3) NOT NULL COMMENT 'teamone id',
	`team_one_points` int(3)  NULL COMMENT 'points accumlated in game',
	`team_two_player` int(3)  NULL COMMENT 'points accumlated in game',
	
	FOREIGN KEY (team_one) REFERENCES Teams(team_id),
	FOREIGN KEY (team_two) REFERENCES Teams(team_id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.statstest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Jeff
 */
public class testing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            URL path = testing.class.getResource("Hockeystats.xlsx");
            FileInputStream file = new FileInputStream(new File(path.getFile()));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                boolean exit = false;
                while (cellIterator.hasNext() && !exit) {

                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.print(cell.getBooleanCellValue() + "\t\t");
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t\t");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            if (cell.getStringCellValue().equals("Scoring")||cell.getStringCellValue().trim().equals("Rk")) {
                                exit = true;
                            } else {
                                System.out.print(cell.getStringCellValue() + "\t\t");
                            }
                            break;
                    }

                }
                if(!exit){
                System.out.println("");}
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(testing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(testing.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

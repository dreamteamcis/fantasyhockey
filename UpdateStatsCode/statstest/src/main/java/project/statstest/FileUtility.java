package project.statstest;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jeff
 * @since 09/25/15 This class contains methods to create, read, and write to
 * files.
 */
public class FileUtility {

    private static Scanner input = new Scanner(System.in);

    /**
     * @author Jeff Neary
     * @since 09/25/15 This method creates the file.
     * @param fileName
     */
    public static void createFile(String fileName) {
        try {
            Path path = Paths.get(fileName);
            Files.createDirectories(path.getParent());
            Files.createFile(path);
        } catch (IOException ex) {
            System.out.println("File already exists.");

        }

    }

    public static Scanner getInput() {
        return input;
    }

    /**
     * @author Jeff Neary
     * @since 09/25/15 This method used the FileChannel Class to write to the
     * end of a file.
     * @param fileName
     * @param deliverable
     */
   

    /**
     * @author Jeff Neary
     * @since 09/25/15 This method Creates a BufferedRedader object. Each line
     * of the file is read. Depending on the boolean value passed into the
     * method a record is added to the array list. The array list is only
     * created at the start of the program to add records back into the list
     * from the file. Otherwise the file is just read from line by line.
     * @param fileName
     * @param addToDeliverables
     * @return
     */
    public static ArrayList readFile(String fileName) {
        ArrayList<String[]> records = new ArrayList<>();
        String aRecord = "";
        Path file = Paths.get(fileName);
        BufferedReader br;
        String [] line;
        try {
            br = new BufferedReader(new InputStreamReader(Files.newInputStream(file)));
            
            while ((aRecord = br.readLine()) != null) {
                    line = aRecord.split(",");
                    records.add(line);
                
            }

        } catch (IOException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return records;
    }

    /**
     * @author Jeff Neary
     * @since 09/25/15 This method is used to create an BasicFileAttributes
     * object incase any attributes of the file are needed. (mainly the size).
     * @param fileName
     * @return
     */
    public static BasicFileAttributes fileAttributes(String fileName) {
        Path file = Paths.get(fileName);
        BasicFileAttributes attr = null;
        try {
            attr = Files.readAttributes(file, BasicFileAttributes.class);

        } catch (IOException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return attr;
    }
}

package info.hccis.jneary.fantasyhockey.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.User;
import info.hccis.jneary.fantasyhockey.util.Exceptions;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;
import info.hccis.jneary.fantasyhockey.util.Utility;
//
public class CreateLeague extends AppCompatActivity {
    EditText leaguename;
    Connect connect;
    Intent intent;
    ServerCallResults results;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_league);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        leaguename = (EditText) findViewById(R.id.editTxtCreateLeague);

    }

    public void createLeagueConfirm(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Create League")
                .setMessage("Are you Sure")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String parameters[] = new String[4];
                        parameters[0] = "userName";
                        parameters[1] = User.getUserName();
                        parameters[2] = "leagueName";
                        try {

                            if (!Utility.verifyUserPassword(leaguename.getText().toString(), "noEntry","null@null.com")) {
                                throw new Exceptions("unusable league name");

                            } else {

                                parameters[3] = leaguename.getText().toString();
                                if (parameters[3].contains(" ")) {
                                    parameters[3]=Utility.formatString(parameters[3]);
                                }
                            }

                            connect = new Connect(parameters, "createLeague.php");
                            connect.execute("");

                            Gson gson = new Gson();
                            ServerCallResults results = new ServerCallResults();
                            try {
                                results = gson.fromJson(connect.get(), ServerCallResults.class);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            } catch (JsonSyntaxException e) {
                                Log.d("test", e.getMessage());
                            }

                            if (results.getStatus().equals("success")) {
                                Toast.makeText(CreateLeague.this, "League Created", Toast.LENGTH_SHORT).show();
                                intent = new Intent(CreateLeague.this, MainActivity.class);
                                startActivity(intent);

                            } else {
                                Toast.makeText(CreateLeague.this, "Could not Create League", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exceptions e) {
                            Log.d("league name exception", e.getMessage());
                            Toast.makeText(CreateLeague.this, "Invalid entry", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                        .setNegativeButton(android.R.string.no, null).show();
                    }
}

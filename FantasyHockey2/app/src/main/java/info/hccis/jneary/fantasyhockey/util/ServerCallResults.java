package info.hccis.jneary.fantasyhockey.util;

/**
 * Created by Jeff on 13/02/2016.
 */
public class ServerCallResults {

    private String status = "fail";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

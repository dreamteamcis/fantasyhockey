package info.hccis.jneary.fantasyhockey.Connection;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import info.hccis.jneary.fantasyhockey.view.Login;


/**
 * Created by Jeff on 09/02/2016.
 * Class name - Connect
 * This class allows the user to create a connection object ie --new Connect(params, "Login.php")--;
 * The first parameter is an array of parameters to be passed into the url, the second
 * is the php script that needs to be run (in this case Login.php). It then returns a JSON object
 * in string form and allows the calling class to pare the data using the GSON class.
 * Date: 9/02/2016
 * Time: 11:59 AM
 */
public class Connect extends AsyncTask<String, String, String> {

    private String link = "http://jneary.hccis.info/";
    private String result = "";
    private HashMap<String, String> webParamsHash = new HashMap<>();
    RequestParams parameters;
    public Connect(){}
    public Connect(String script) {
        link += script;

        webParamsHash.put("null", "null");

    }

    public Connect(String[] webParams, String script) {
        String key = "";
        String value = "";
        link += script;
        for (int i = 0; i < webParams.length; i++) {
            if (i % 2 == 0) {
                key = webParams[i];
                value = webParams[i + 1];
            }
            webParamsHash.put(key, value);
        }
        parseParams();
    }
    /**
     * Method Name : parseParams
     * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
     * Purpose: Parses the params and adds them to the link.
     * Date: 9/02/2016
     * Time: 11:59 AM
     */
public void parseParams(){
    Object[] paramNames = webParamsHash.keySet().toArray();
    Object[] allParams=webParamsHash.values().toArray();
    for(int i=0;i<webParamsHash.size();i++){

        if(i==0&&webParamsHash.size()>1){
            link+="?"+paramNames[i].toString()+"="+allParams[i].toString()+"&";
        }
        else if(i!=0&&i!=webParamsHash.size()-1){
            link+=paramNames[i].toString()+"="+allParams[i].toString()+"&";
        }
        else if(i==webParamsHash.size()-1&&i!=0){
            link+=paramNames[i].toString()+"="+allParams[i].toString();
        }
        else if(i==0){
            link+="?"+paramNames[i].toString()+"="+allParams[i].toString();
        }



    }
}
    /**
     * Method Name : executeConnection
     * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
     * Purpose: This method is not used. Could never figure out how to get the main thread to stop while this
     * was thread was running/
     * Date: 09/02/2016
     * Time: 11:59 AM
     */
    public String executeConnection(Context context, ProgressDialog progress) {

        parameters = new RequestParams();
        for (Map.Entry<String, String> val : webParamsHash.entrySet()) {
            parameters.put(val.getKey(), val.getValue());
        }

        // Show Progress Dialog

        // Make  webservice call using AsyncHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        //passest parameters to web client and set up handler that listens for respons.

        AsyncHttpResponseHandler a = new AsyncHttpResponseHandler() {


            /**
             * Method Name : onSuccess
             * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
             * Purpose: triggered if connection to client is a success. Then converts JSON
             * object to a string and responds accordingly based on passed in credentials.
             * Date: 24/01/2016
             * Time: 11:59 AM
             */

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d("testing login success", "worked ");
                try {
                    String stringResponse = new String(responseBody, "UTF-8");
                    Login.getProgress().hide();
                    Log.d("testing login success", stringResponse);
                    JSONObject resp = new JSONObject(stringResponse);
                    Log.d("testing valiue", resp.get("status").toString());
                    result = resp.get("status").toString();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            /**
             * Method Name : onFailure
             * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
             * Purpose: If unable to connect to client and error message is returned.
             * Date: 24/01/2016
             * Time: 11:59 AM
             */
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("testing login success", "Could not connect to client ");
                //progress.hide();
            }

            @Override
            public void onStart() {
                super.onStart();
            }
        };

       client.get(link, parameters, a);



        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Login.getProgress().show();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Login.getProgress().hide();
    }
    /**
     * Method Name : doInBackground
     * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
     * Purpose: background thread. pauses main thread until a connection  is established and
     * a value is returned.
     * Date: 24/01/2016
     * Time: 11:59 AM
     */
    @Override
    protected String doInBackground(String... params) {

            StringBuilder result = new StringBuilder(); //sets return string
            HttpURLConnection urlConnection = null; //sets connection
            try {
                URL url = new URL(link); //create url

                urlConnection = (HttpURLConnection) url.openConnection(); //open the connection
                urlConnection.connect(); //connect to internet
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream()); //pass results to input stream
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream)); //read input stream


                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line); //pass reader data to string builder
                }
            } catch (MalformedURLException e) {
                Log.d("Malformed URL", e.toString());
            } catch (IOException e) {
                Log.d("IOException", e.toString());
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect(); //close connection
                }
            }
            return result.toString(); //return results as string
        }

}

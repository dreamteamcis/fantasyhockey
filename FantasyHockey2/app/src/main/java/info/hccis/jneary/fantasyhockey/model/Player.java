package info.hccis.jneary.fantasyhockey.model;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Jeff on 14/02/2016.
 */
public class Player implements Serializable {
    private List<Player> players;
    private float playerId;
    private String playerName;
    private float playerPoints;
    private float playerGoals;
    private float team ;
    private float gamesPlayed;

    public Player(){

    }

    public Player(float playerId, String playerName, float playerPoints, float playerGoals, float team, float gamesPlayed) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerPoints = playerPoints;
        this.playerGoals = playerGoals;
        this.team = team;
        this.gamesPlayed = gamesPlayed;
    }

    public List<Player> getPlayers() {
        return players;
    }


    public void setPlayers(List<Player> players) {
        this.players = players;
    }
    public void parseMap(Map<String,Object> map){
        players = new ArrayList<Player>();
        for (Object value : map.values()) {
            String temp =  value.toString();
            ArrayList<String> list = new ArrayList<>();

            Log.d("test", "parseMap: "+temp);
            String[] temp2 = temp.split(",");
            String[] temp3;
            for(int i=0;i<temp2.length;i++){
                temp3 = temp2[i].split("=");
                if(i==5){
                   temp3[1]= temp3[1].replace("}", "");
                }
                list.add(temp3[1]);
            }
            Player aPlayer = new Player(Float.parseFloat(list.get(0)),list.get(1),
                    Float.parseFloat(list.get(2)),Float.parseFloat(list.get(3)),Float.parseFloat(list.get(4)),
                    Float.parseFloat(list.get(5)));
            players.add(aPlayer);



        }
    }
    public float getPlayerId() {
        return playerId;
    }

    public void setPlayerId(float playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public float getPlayerPoints() {
        return playerPoints;
    }

    public void setPlayerPoints(float playerPoints) {
        this.playerPoints = playerPoints;
    }

    public float getPlayerGoals() {
        return playerGoals;
    }

    public void setPlayerGoals(float playerGoals) {
        this.playerGoals = playerGoals;
    }

    public float getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public float getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(float gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public Player returnSinglePlayer(int postition){
        Player singlePlayer;

            singlePlayer = players.get(postition);
            return  singlePlayer;
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerId=" + playerId +
                ", playerName='" + playerName + '\'' +
                ", playerPoints=" + playerPoints +
                ", playerGoals=" + playerGoals +
                ", team=" + team +
                ", gamesPlayed=" + gamesPlayed +
                ", players=" + players +
                '}';
    }
    public String[] returnPlayers(){
        String[] names = new String[players.size()];
        for(int i = 0;i<players.size();i++){
            names[i]=players.get(i).getPlayerName();
        }
        return names;
    }
}

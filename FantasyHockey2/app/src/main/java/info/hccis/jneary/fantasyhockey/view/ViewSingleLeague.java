package info.hccis.jneary.fantasyhockey.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.User;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;

public class ViewSingleLeague extends AppCompatActivity {
    TextView leagueName;
    TextView draftStarted;
    TextView numberOfTeams;
    Connect connect;
    String leagueId;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_single_league);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getExtras().get("error")!=null){
            leagueName.setText(getIntent().getExtras().get("error").toString());
        }
        else{
        numberOfTeams = (TextView) findViewById(R.id.txtViewNumberOfTeams);
        draftStarted = (TextView) findViewById(R.id.txtViewLeagueDraft);
        leagueName = (TextView) findViewById(R.id.txtViewLeagueName);
        numberOfTeams.setText(getIntent().getExtras().get("numberOfTeams").toString());
        draftStarted.setText(getIntent().getExtras().get("draftStarted").toString());
        leagueName.setText(getIntent().getExtras().get("leagueName").toString());
        leagueId=getIntent().getExtras().get("leagueID").toString();}
    }

    public void joinLeague(View view) {

        new AlertDialog.Builder(this)
                .setTitle("Join League")
                .setMessage("Are you Sure")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String parameters[] = new String[4];
                        parameters[0] = "userName";
                        parameters[1] = User.getUserName();
                        parameters[2] = "leagueId";
                        parameters[3] = leagueId;
                        connect = new Connect(parameters, "joinLeague.php");
                        connect.execute("");
                        Gson gson = new Gson();
                        ServerCallResults results = new ServerCallResults();
                        try {
                            results = gson.fromJson(connect.get(), ServerCallResults.class);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (JsonSyntaxException e) {
                            Log.d("test", e.getMessage());
                        }
                        if (results.getStatus().equals("success")) {
                            Toast.makeText(ViewSingleLeague.this, "League Joined", Toast.LENGTH_SHORT).show();
                            intent = new Intent(ViewSingleLeague.this,MainActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(ViewSingleLeague.this, "Could not Join", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }
}

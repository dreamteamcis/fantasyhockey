package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.User;

public class MainActivity extends AppCompatActivity {
    private TextView heading;
    private String[] parameters = new String[2];
    private Connect connect;
    private TextView teamOneScore;
    private TextView teamTwoScore;
    private TextView teamOneName;
    private TextView teamTwoName;
    private TextView txtViewMainPageHeading;
    private LinearLayout scoringInfo;
    private LinearLayout scoringInfo2;
    private Button btnDraft;
    private Button btnMatches;
    private Button btnStandings;
    private Button btnViewLeagues;
    private Intent intent;
    private Gson gson;
    private User aUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Main Menu");
        teamOneName = (TextView) findViewById(R.id.teamOne);
        teamTwoName = (TextView) findViewById(R.id.teamTwo);
        teamOneScore = (TextView) findViewById(R.id.teamOneScore);
        teamTwoScore = (TextView) findViewById(R.id.teamTwoScore);
        heading = (TextView) findViewById(R.id.txtViewMainPageHeading);
        scoringInfo = (LinearLayout) findViewById(R.id.scoringInfo);
        scoringInfo2 = (LinearLayout) findViewById(R.id.scoringInfo2);
        btnDraft = (Button) findViewById(R.id.btnDraft);
        btnMatches =(Button) findViewById(R.id.btnMatches);
        btnStandings =(Button) findViewById(R.id.btnStandings);
        btnViewLeagues = (Button) findViewById(R.id.btnViewLeagues);
        heading.setText("Matches");
        parameters[0]="userName";
        parameters[1]= User.getUserName();
        connect = new Connect(parameters,"CheckUserPrivileges.php");
        connect.execute("");
        Gson gson = new Gson();
        aUser = new User();
        try {


            aUser=gson.fromJson(connect.get(), User.class);
            //Log.d("GSON", "result gson "+temp);
            //String testing = temp.toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }catch (JsonSyntaxException e){
            Log.d("test", e.getMessage());
        }
        teamOneName.setText(aUser.getTeamOne());
        teamTwoName.setText(aUser.getTeamTwo());
        teamOneScore.setText(aUser.getTeamOneScore());
        teamTwoScore.setText(aUser.getTeamTwoScore());
        if(teamOneScore.getText().equals("false")){
            txtViewMainPageHeading = (TextView) findViewById(R.id.txtViewMainPageHeading);
            txtViewMainPageHeading.setText("No Matches");
            scoringInfo.setVisibility(View.INVISIBLE);
            scoringInfo2.setVisibility(View.INVISIBLE);
        }
        if(aUser.getLeague()==null){
            aUser.setLeague("false");
        }
        if(!aUser.getLeague().equals("false")){
            btnViewLeagues.setVisibility(View.INVISIBLE);


        }else if(aUser.getLeague().equals("false"))
        {
            btnStandings.setVisibility(View.INVISIBLE);
            btnMatches.setVisibility(View.INVISIBLE);
        }
        if(aUser.getDraft().equals("false")){
            btnDraft.setVisibility(View.INVISIBLE);

        }
        if(aUser.getLeagueOwner().equals("true")&&aUser.getDraft().equals("false")&& aUser.getDraftUserTurn().equals("0")&&aUser.getSeasonStarted().equals("0")){
            btnDraft.setVisibility(View.VISIBLE);
        }


    }

    public void goToLeagues(View view) {
        intent = new Intent(MainActivity.this,ViewLeague.class);
        startActivity(intent);
    }

    public void beginDraft(View view) {
        intent = new Intent(MainActivity.this,Draft.class);

        intent.putExtra("draft",aUser.getDraft());
        intent.putExtra("numberOfTeams",aUser.getNumberOfTeams());
        intent.putExtra("playersTurn",aUser.getDraftUserTurn());
        startActivity(intent);

    }

    public void viewMatches(View view) {
        if(aUser.getSeasonStarted().equals("0")){
            Toast.makeText(MainActivity.this, "Season not started", Toast.LENGTH_SHORT).show();
        }else{
        intent = new Intent(MainActivity.this,ViewMatches.class);
        intent = intent.putExtra("leagueId",aUser.getLeague());
        intent = intent.putExtra("leagueOwner",aUser.getLeagueOwner());
        startActivity(intent);}
    }

    public void viewStandings(View view) {
        if(aUser.getSeasonStarted().equals("0")){
            Toast.makeText(MainActivity.this, "Season not started", Toast.LENGTH_SHORT).show();
        }else{
        intent = new Intent(MainActivity.this, ViewStandings.class);
        intent.putExtra("leagueId", aUser.getLeague());
        startActivity(intent);}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 39876, Menu.NONE,"Refresh").setIcon((R.drawable.ic_action_name));
        return super.onCreateOptionsMenu(menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==39876){
            Log.d("tesing icon", "onOptionsItemSelected: ");
            intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

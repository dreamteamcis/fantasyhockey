package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.League;

public class ViewLeague extends AppCompatActivity {
    League league;
    String message="";
    Intent intent;
    Connect connect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_league);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Connect connect = new Connect("getLeagues.php");
        connect.execute("");

        try{
            Gson gson = new Gson();
            league = new League();
            league = gson.fromJson(connect.get(),League.class);
            //league.setLeagues(abc);

        }catch (InterruptedException | ExecutionException e) {

            e.printStackTrace();

        }
        if(league.getLeagues()==null){
            message = "No Results";
            intent = new Intent(ViewLeague.this,ViewSingleLeague.class);
            intent.putExtra("error","No Leagues");
            startActivity(intent);

        }else{


            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    league.returnTitles());
            ListView leagueList = (ListView) findViewById(R.id.leagueList);
            leagueList.setAdapter(listAdapter);
            leagueList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String draftStarted;
                    intent = new Intent(ViewLeague.this,ViewSingleLeague.class);
                    intent.putExtra("leagueID",league.getLeagues().get(position).getLeagueId());
                    if(!league.getLeagues().get(position).isDraftStarted()){
                        draftStarted="No";
                    }
                    else{
                        draftStarted="Yes";
                    }
                    intent.putExtra("draftStarted",draftStarted);
                    intent.putExtra("leagueName", league.getLeagues().get(position).getLeagueName());
                    Log.d("leagues", "League name= "+league.getLeagues().get(position).getLeagueName());
                    intent.putExtra("numberOfTeams",league.getLeagues().get(position).getNumberOfTeams());
                    startActivity(intent);
                }
            });
        }


    }

    public void createLeague(View view) {
        intent = new Intent(ViewLeague.this,CreateLeague.class);
        startActivity(intent);
    }
}

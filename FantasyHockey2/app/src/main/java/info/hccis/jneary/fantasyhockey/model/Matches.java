package info.hccis.jneary.fantasyhockey.model;

import java.util.List;

/**
 * Created by Jeff on 17/02/2016.
 */
public class Matches {
    private List<Matches> matches;
    private int matchId;
    private int teamOne;
    private int teamTwo;
    private int teamOnePoints;
    private int teamTwoPoints;
    private String teamOneName;
    private String teamTwoName;

    public Matches(){

    }

    public Matches(int matchId, int teamOne, int teamTwo, int teamOnePoints, int teamTwoPoints,String teamOneName,String teamTwoName) {
        this.matchId = matchId;
        this.teamOne = teamOne;
        this.teamTwoName = teamTwoName;
        this.teamOneName = teamOneName;
        this.teamTwo = teamTwo;
        this.teamOnePoints = teamOnePoints;
        this.teamTwoPoints = teamTwoPoints;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getTeamOne() {
        return teamOne;
    }

    public void setTeamOne(int teamOne) {
        this.teamOne = teamOne;
    }

    public int getTeamTwo() {
        return teamTwo;
    }

    public void setTeamTwo(int teamTwo) {
        this.teamTwo = teamTwo;
    }

    public int getTeamOnePoints() {
        return teamOnePoints;
    }

    public void setTeamOnePoints(int teamOnePoints) {
        this.teamOnePoints = teamOnePoints;
    }

    public int getTeamTwoPoints() {
        return teamTwoPoints;
    }

    public void setTeamTwoPoints(int team_two_points) {
        this.teamTwoPoints = team_two_points;
    }

    public String getTeamOneName() {
        return teamOneName;
    }

    public void setTeamOneName(String teamOneName) {
        this.teamOneName = teamOneName;
    }

    public String getTeamTwoName() {
        return teamTwoName;
    }

    public void setTeamTwoName(String teamTwoName) {
        this.teamTwoName = teamTwoName;
    }

    public List<Matches> getMatches() {
        return matches;
    }

    public void setMatches(List<Matches> matches) {
        this.matches = matches;
    }
}

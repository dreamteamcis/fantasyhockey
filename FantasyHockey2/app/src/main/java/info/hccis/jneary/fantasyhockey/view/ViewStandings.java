package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.Team;
import info.hccis.jneary.fantasyhockey.util.Exceptions;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;

public class ViewStandings extends AppCompatActivity {
    Connect connection;
    TableLayout tableLayout;
    String errorMessage;
    TextView teamText;
    Intent intent;
    String leagueId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //R.layout.activity_view_standings
        setContentView(R.layout.activity_view_standings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        teamText = (TextView) findViewById(R.id.standingsTeamName);
        tableLayout = (TableLayout) findViewById(R.id.tableStandings);
        ViewGroup.LayoutParams params2 = teamText.getLayoutParams();
        final String[] params = new String[2];
        leagueId=getIntent().getExtras().get("leagueId").toString();
        params[0] = "leagueId";
        params[1] = leagueId;
        connection = new Connect(params, "viewStandings.php");
        connection.execute("");
        Gson gson = new Gson();
        Team team = new Team();
        TableLayout.LayoutParams textViewParam = new TableLayout.LayoutParams
                (TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
        try {
            team = gson.fromJson(connection.get(), Team.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (team.getStandings() == null) {
            errorMessage = "No results";
        } else {
            for (final Team aTeam : team.getStandings()) {
                TableRow tableRow1 = new TableRow(this);
                tableRow1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("ClickTeam", "onClick: ");
                        int number = v.getId();
                        intent = new Intent(ViewStandings.this, ViewTeam.class);
                        intent.putExtra("teamId", aTeam.getTeamId());
                        intent.putExtra("teamName", aTeam.getTeamName());
                        intent.putExtra("leagueId",params[1]);
                        startActivity(intent);
                    }
                });
                tableRow1.setLayoutParams(new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

                TextView teams = new TextView(this);
                teams.setTextColor(Color.WHITE);
                teams.setMinHeight(teamText.getHeight());
                teams.setMinWidth(teamText.getWidth());
                teams.setGravity(Gravity.CENTER);
                teams.setGravity(teamText.getGravity());
                teams.setTextSize(20);
                teams.setLayoutParams(params2);
                teams.setText(aTeam.getTeamName());
                teams.setEllipsize(TextUtils.TruncateAt.END);
                teams.setMaxLines(1);
                TextView wins = new TextView(this);
                wins.setTextColor(Color.WHITE);
                wins.setMinHeight(teamText.getHeight());
                wins.setMinWidth(teamText.getWidth());
                wins.setTextSize(20);
                wins.setGravity(Gravity.CENTER);
                wins.setText(String.valueOf(aTeam.getWins()));
                wins.setLayoutParams(params2);
                wins.setEllipsize(TextUtils.TruncateAt.END);

                wins.setMaxLines(1);
                tableRow1.addView(teams);
                tableRow1.addView(wins);
                tableLayout.addView(tableRow1);



            }
        }

    }

    public void refreshStats(View view) {


        connection = new Connect("ExelImport/updateDatabase.php");
        connection.execute("");
        Gson gson = new Gson();
        ServerCallResults scr = new ServerCallResults();
        try{
            scr = gson.fromJson(connection.get(),ServerCallResults.class);
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();

        }
        catch (JsonSyntaxException e){
            Log.d("test", e.getMessage());
            errorMessage = "true";
        }
        try{
            if (scr.getStatus().equals("fail")){
                throw new Exceptions("Could not refresh");
            }else{
                Toast.makeText(getApplicationContext(),
                        "Stats updated!", Toast.LENGTH_LONG).show();
            }
        }catch (Exceptions e){
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }
        Intent intent = new Intent(ViewStandings.this,MainActivity.class);
        startActivity(intent);
    }
}

package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.Matches;
import info.hccis.jneary.fantasyhockey.util.Exceptions;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;

public class ViewMatches extends AppCompatActivity {
    private Connect connection;
    private TableLayout tableLayout;
    private String errorMessage;
    private TextView teamOneText;
    private String leagueId;
    private TextView heading;
    private Button endWeek;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_matches);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        heading = (TextView)findViewById(R.id.txtViewMatchesList);
        teamOneText = (TextView) findViewById(R.id.matchesTeamOne);
        tableLayout = (TableLayout) findViewById(R.id.matches_table);
        ViewGroup.LayoutParams params2 = teamOneText.getLayoutParams();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        errorMessage="false";
        endWeek = (Button)findViewById(R.id.btnEndWeek);
        if(getIntent().getExtras().get("leagueOwner")!=null){
        String leagueOwner = getIntent().getExtras().get("leagueOwner").toString();
        if(leagueOwner.equals("false")){
            endWeek.setVisibility(View.INVISIBLE);
        }}
        //LinearLayout layout = (LinearLayout) findViewById(R.id.matchesLinearLayout);
        leagueId = getIntent().getExtras().get("leagueId").toString();
        String[] params = new String[2];
        params[0] = "leagueId";
        params[1] = leagueId;
        connection = new Connect(params,"getMatches.php");
        connection.execute("");
        Gson gson = new Gson();
        Matches matches = new Matches();
        TableLayout.LayoutParams textViewParam = new TableLayout.LayoutParams
                (TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
        try {
            matches = gson.fromJson(connection.get(),Matches.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        catch (JsonSyntaxException e){
            Log.d("test", e.getMessage());
            errorMessage = "true";
        }
        if(matches.getMatches()==null || errorMessage.equals("true")){
                errorMessage = "No results";
                heading.setText(errorMessage);

            }else {
                for(int i=0;i<matches.getMatches().size();i++) {
                    TableRow tableRow1 = new TableRow(this);
                    tableRow1.setLayoutParams(new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    TableRow tableRow2 = new TableRow(this);
                    TextView teamOne = new TextView(this);

                    teamOne.setMinHeight(teamOneText.getHeight());
                    teamOne.setMinWidth(teamOneText.getWidth());
                    teamOne.setGravity(Gravity.CENTER);
                    //teamOne.setGravity(teamOneText.getGravity());
                    teamOne.setTextSize(20);
                    teamOne.setTextColor(Color.WHITE);
                    teamOne.setMaxWidth(20);
                    teamOne.setEllipsize(TextUtils.TruncateAt.END);
                    teamOne.setMaxLines(1);
                    teamOne.setLayoutParams(params2);
                    teamOne.setText(matches.getMatches().get(i).getTeamOneName());
                    TextView teamTwo = new TextView(this);
                    teamTwo.setMinHeight(teamOneText.getHeight());
                    teamTwo.setMinWidth(teamOneText.getWidth());
                    teamTwo.setTextSize(20);
                    teamTwo.setTextColor(Color.WHITE);
                    teamTwo.setMaxWidth(20);
                    teamTwo.setGravity(Gravity.CENTER);
                    teamTwo.setText(matches.getMatches().get(i).getTeamTwoName());
                    teamTwo.setLayoutParams(params2);
                    TextView teamOneScore = new TextView(this);
                    teamOneScore.setLayoutParams(params2);
                    teamOneScore.setGravity(Gravity.CENTER);
                    teamOneScore.setTextColor(Color.WHITE);
                    teamOneScore.setText(String.valueOf(matches.getMatches().get(i).getTeamOnePoints()));
                    TextView teamTwoScore = new TextView(this);
                    teamTwoScore.setGravity(Gravity.CENTER);
                    teamTwoScore.setLayoutParams(params2);
                    teamTwoScore.setTextColor(Color.WHITE);
                    teamTwoScore.setText(String.valueOf(matches.getMatches().get(i).getTeamTwoPoints()));
                    tableRow1.addView(teamOne);
                    tableRow1.addView(teamTwo);
                    tableRow2.addView(teamOneScore);
                    tableRow2.addView(teamTwoScore);
                    tableLayout.addView(tableRow1);
                    tableLayout.addView(tableRow2);


                }}


    }

    public void endWeek(View view) {
        String[] params = new String[2];
        params[0] = "leagueId";
        params[1] = leagueId;
        connection = new Connect(params,"endWeek.php");
        connection.execute("");
        Gson gson = new Gson();
        ServerCallResults scr = new ServerCallResults();
        try{
            scr = gson.fromJson(connection.get(),ServerCallResults.class);
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();

        }
        catch (JsonSyntaxException e){
            Log.d("test", e.getMessage());
            errorMessage = "true";
        }
        try{
        if (scr.getStatus().equals("fail")){
            throw new Exceptions("could not start week");
        }else{
            Toast.makeText(getApplicationContext(),
                    "New week started!", Toast.LENGTH_LONG).show();
        }
        }catch (Exceptions e){
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }
        Intent intent = new Intent(ViewMatches.this,MainActivity.class);
        startActivity(intent);
    }
}

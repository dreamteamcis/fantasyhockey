package info.hccis.jneary.fantasyhockey.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.User;
import info.hccis.jneary.fantasyhockey.util.Exceptions;
import info.hccis.jneary.fantasyhockey.util.Utility;

/**
 * Class Name : Login
 * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
 * Purpose: Allows user to login. Connects to the database
 * and determines if user credentials are correct.
 * Date: 24/01/2016
 * Time: 11:59 AM
 */
public class Login extends AppCompatActivity {
    private EditText userName;
    private EditText password;
    private static ProgressDialog progress;
    private TextView errorMessage;
    private Intent intent;
    private Connect connect;
    private String[] params = new String[4];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView image = (ImageView) findViewById(R.id.imageView);
        image.setImageResource(R.drawable.logo_dark);

        getSupportActionBar().setTitle("Login");
        userName = (EditText) findViewById(R.id.editTxtUserName);
        password = (EditText) findViewById(R.id.editTxtPassword);

        errorMessage = (TextView) findViewById(R.id.errorMessage);
        progress = new ProgressDialog(this);
        progress.setMessage("Loading.......");
        progress.setCancelable(false);



    }

    public static ProgressDialog getProgress() {
        return progress;
    }

    /**
     * Method Name : loginAttempt
     * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson.
     * Purpose: Passes web parameters username and password int a php file that
     * checks those credentials against the database. A JSON object is returned.
     * Date: 24/01/2016
     * Time: 11:59 AM
     */

    public void loginAttempt(View view) {
        try{
            params[0] = "userName";
            params[1] = userName.getText().toString();
            params[2] = "password";
            params[3] = password.getText().toString();
            connect = new Connect(params, "Login.php");

            if(!Utility.verifyUserPassword(params[1],params[3],"null@null.com")){
                throw new Exceptions("Username or password was invalid");
            }



            //String test = connect.executeConnection(view.getContext(),progress);
            connect.execute("");
            Gson gson = new Gson();
            User aUser = new User();
            try {

                aUser=gson.fromJson(connect.get(), User.class);
                //Log.d("GSON", "result gson "+temp);
                //String testing = temp.toString();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            //Log.d("login", "loginAttempt: "+test);

            if (aUser.getStatus().equals("fail")) {
                errorMessage.setText("Incorrect username or password");
                Toast.makeText(getApplicationContext(),
                        "Login failure", Toast.LENGTH_LONG).show();
            } else {
                errorMessage.setText("");
                User.setUserName(userName.getText().toString());
                intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(getApplicationContext(),
                        "Login success", Toast.LENGTH_LONG).show();
            }


        }catch (Exceptions e){
            Toast.makeText(getApplicationContext(),
                    "Invalid Username or password", Toast.LENGTH_LONG).show();
        }}


    public void navigatCreateAccount(View view) {
        intent = new Intent(Login.this,CreateAccount.class);
        startActivity(intent);
    }
}
package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Size;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.Player;
import info.hccis.jneary.fantasyhockey.model.Team;

public class ViewTeam extends AppCompatActivity {
    TextView playerName;
    TextView goalsText;
    TextView gamesText;
    TextView pointsText;
    TableLayout tableLayout;
    TextView teamHeading;
    Connect connection;
    String errorMessage;
    String leagueId;
    String teamName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_team);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(R.drawable.gradient_button_yellow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        leagueId = getIntent().getExtras().get("leagueId").toString();
        teamName = getIntent().getExtras().get("teamName").toString();
        teamHeading = (TextView) findViewById(R.id.viewTeamHeading);
        playerName = (TextView) findViewById(R.id.ViewTeamPlayerName);
        goalsText = (TextView)findViewById(R.id.ViewTeamPlayerGoals);
        gamesText = (TextView)findViewById(R.id.ViewTeamPlayerGames);
        pointsText = (TextView)findViewById(R.id.ViewTeamPlayerPoints);
        tableLayout = (TableLayout) findViewById(R.id.tableViewTeam);
        ViewGroup.LayoutParams params2 = playerName.getLayoutParams();
        teamHeading.setText(getIntent().getExtras().get("teamName").toString());
        teamHeading.setTextSize(30);
        teamHeading.setTextColor(Color.BLACK);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String[] params = new String[2];
        params[0] = "teamId";
        params[1] = getIntent().getExtras().get("teamId").toString();
        connection = new Connect(params, "ViewTeam.php");
        connection.execute("");
        Gson gson = new Gson();
        Player player = new Player();
        TableLayout.LayoutParams textViewParam = new TableLayout.LayoutParams
                (TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
        try {
            player = gson.fromJson(connection.get(), Player.class);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (player.getPlayers() == null) {
            errorMessage = "No results";
        } else {
            for (final Player aPlayer : player.getPlayers()) {
                TableRow tableRow1 = new TableRow(this);
                tableRow1.setLayoutParams(new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

                TextView playerNames = new TextView(this);
                playerNames.setMinHeight(playerName.getHeight());
                playerNames.setMinWidth(playerName.getWidth());
                playerNames.setGravity(Gravity.CENTER);
                playerNames.setGravity(playerName.getGravity());
                playerNames.setTextSize(14);
                playerNames.setLayoutParams(params2);
                playerNames.setText(aPlayer.getPlayerName());
                playerNames.setEllipsize(TextUtils.TruncateAt.END);
                playerNames.setMaxLines(1);
                playerNames.setTextColor(Color.WHITE);
                ;
                TextView playerGames = new TextView(this);
                playerGames.setMinHeight(playerName.getHeight());
                playerGames.setMinWidth(playerName.getWidth());
                playerGames.setTextSize(14);
                playerGames.setGravity(Gravity.CENTER);
                playerGames.setText(String.valueOf(aPlayer.getGamesPlayed()));
                playerGames.setLayoutParams(params2);
                playerGames.setEllipsize(TextUtils.TruncateAt.END);
                playerGames.setTextColor(Color.WHITE);

                playerGames.setMaxLines(1);
                TextView playerGoals = new TextView(this);
                playerGoals.setMinHeight(playerName.getHeight());
                playerGoals.setMinWidth(playerName.getWidth());
                playerGoals.setTextSize(14);
                playerGoals.setGravity(Gravity.CENTER);
                playerGoals.setText(String.valueOf(aPlayer.getPlayerGoals()));
                playerGoals.setLayoutParams(params2);
                playerGoals.setEllipsize(TextUtils.TruncateAt.END);

                playerGoals.setMaxLines(1);
                playerGoals.setTextColor(Color.WHITE);

                TextView playerPoints = new TextView(this);
                playerPoints.setMinHeight(playerName.getHeight());
                playerPoints.setMinWidth(playerName.getWidth());
                playerPoints.setTextSize(14);
                playerPoints.setGravity(Gravity.CENTER);
                playerPoints.setText(String.valueOf(aPlayer.getPlayerPoints()));
                playerPoints.setLayoutParams(params2);
                playerPoints.setEllipsize(TextUtils.TruncateAt.END);
                playerPoints.setMaxLines(1);
                playerPoints.setTextColor(Color.WHITE);
                tableRow1.addView(playerNames);
                tableRow1.addView(playerGames);
                tableRow1.addView(playerGoals);
                tableRow1.addView(playerPoints);
                tableLayout.addView(tableRow1);
            }

        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(ViewTeam.this, ViewStandings.class);
                intent.putExtra("leagueId", leagueId);
                NavUtils.navigateUpTo(this, intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

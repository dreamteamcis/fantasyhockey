package info.hccis.jneary.fantasyhockey.util;

import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jeff on 10/02/2016.
 */
public class Utility {
       private static MessageDigest di;
    public static String hashPassword(String password){
        try {
            di = MessageDigest.getInstance("MD5");
            byte[] bytePassword = di.digest(password.getBytes());
            BigInteger adjustedPassword = new BigInteger(1, bytePassword);
            String hashedValue = adjustedPassword.toString(16);

            while (hashedValue.length() < 32) {
                hashedValue = "0" + hashedValue;
            }
            return hashedValue;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

    }
    public static boolean sanitizeTeamName(String teamname){
        if(teamname.length()>9){
            return false;
        }
        return  true;
    }
    public static boolean verifyUserPassword(String userName,String password,String email){
        String regexEmail = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        Pattern emailFormat = Pattern.compile(regexEmail);
        Matcher matcher = emailFormat.matcher(email);
        if(userName.contains("\\")||userName.contains("'")||userName.contains("\"")||
                password.contains("\\")||password.contains("'")||password.contains("\"")){
            return false;
        }
        if(!matcher.matches()){
            Log.d("Email", "EmailFailed: ");
            return false;
        }
        if(userName.length()>20||password.length()>20||userName.length()<3||password.length()<3){
            return false;
        }
        if(userName.trim().equals("")||password.trim().equals("")){
            return false;

        }
        else{
            return true;
        }

    }
    public static String formatString(String data){
        String formatedString = "";
        formatedString = data.trim().replaceAll(" +", " ");
        formatedString=data.replaceAll("\\s+","-");
        return formatedString;
    }
    public static int randomNumber(int min,int max){
        int range = (max - min) + 1;
        int number =  (int)(Math.random() * range) + min;
        return  number;
    }
}


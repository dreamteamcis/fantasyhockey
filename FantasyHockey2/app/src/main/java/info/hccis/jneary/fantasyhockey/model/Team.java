package info.hccis.jneary.fantasyhockey.model;

import java.util.List;

/**
 * Created by Jay on 2016-02-18.
 */
public class Team {
    private int teamId;
    private String teamName;
    private int wins;
    private List<Team> standings;

    public List<Team> getStandings() {
        return standings;
    }

    public void setStandings(List<Team> standings) {
        this.standings = standings;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }
}

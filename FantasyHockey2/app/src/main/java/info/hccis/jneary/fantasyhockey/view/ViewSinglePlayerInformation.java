package info.hccis.jneary.fantasyhockey.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.Player;
import info.hccis.jneary.fantasyhockey.model.User;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;

public class ViewSinglePlayerInformation extends AppCompatActivity {
    Player player;
    Connect connect;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_single_player_information);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView playerName = (TextView) findViewById(R.id.txtViewPlayerName);
        TextView playerPoints = (TextView) findViewById(R.id.txtViewPlayerPoints2);
        TextView playerGoals = (TextView) findViewById(R.id.txtViewPlayerGoals);
        TextView playerGames = (TextView) findViewById(R.id.txtViewPlayerGames);
        player= (Player) getIntent().getExtras().get("player");
        playerName.setText(player.getPlayerName());
        playerPoints.setText(String.valueOf(player.getPlayerPoints()));
        playerGoals.setText(String.valueOf(player.getPlayerGoals()));
        playerGames.setText(String.valueOf(player.getGamesPlayed()));
        toolbar.setTitle(player.getPlayerName());
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("testing", "onBackPressed: ");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ;

        return super.onOptionsItemSelected(item);

    }

    public void addPlayer(View view) {

        new AlertDialog.Builder(this)
                .setTitle("Add Player")
                .setMessage("Are you Sure")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String parameters[] = new String[4];
                        parameters[0] = "userName";
                        parameters[1] = User.getUserName();
                        parameters[2] = "playerId";
                        parameters[3] = String.valueOf(player.getPlayerId());
                        connect = new Connect(parameters, "addPlayer.php");
                        connect.execute("");
                        Gson gson = new Gson();
                        ServerCallResults results = new ServerCallResults();
                        try {
                            results = gson.fromJson(connect.get(), ServerCallResults.class);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (JsonSyntaxException e) {
                            Log.d("test", e.getMessage());
                        }
                        if (results.getStatus().equals("success")) {
                            Toast.makeText(ViewSinglePlayerInformation.this, "Player Added", Toast.LENGTH_SHORT).show();
                            intent = new Intent(ViewSinglePlayerInformation.this,MainActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(ViewSinglePlayerInformation.this, "Could not add Player", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }
}

package info.hccis.jneary.fantasyhockey.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jeff on 09/02/2016.
 */
public class User implements Serializable{

    private static String userName;
    private String draft;
    private String seasonStarted = "";
    private  String team = "";
    private  String league = "";
    private  String leagueOwner = "";
    private  String status = "";
    private  String teamOne = "";
    private  String teamTwo = "";
    private  String teamOneScore = "";
    private  String teamTwoScore = "";
    private String draftUserTurn ="";
    private int numberOfTeams;
    private List<User> users;
    public User(){}
    public User(String draft, String team, String league, String leagueOwner,
                String status, String teamOne, String teamTwo, String teamOneScore,
                String teamTwoScore, List<User> users) {
        this.draft = draft;
        this.team = team;
        this.league = league;
        this.leagueOwner = leagueOwner;
        this.status = status;
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
        this.teamOneScore = teamOneScore;
        this.teamTwoScore = teamTwoScore;
        this.users = users;
    }

    public String getLeagueOwner() {
        return leagueOwner;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getDraftUserTurn() {
        return draftUserTurn;
    }

    public void setDraftUserTurn(String draftUserTurn) {
        this.draftUserTurn = draftUserTurn;
    }

    public static String getUserName() {
        return userName;
    }

    public String getStatus() {
        return status;
    }

    public int getNumberOfTeams() {
        return numberOfTeams;
    }

    public void setNumberOfTeams(int numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static void setUserName(String userName) {
        User.userName = userName;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String isLeagueOwner() {
        return leagueOwner;
    }

    public void setLeagueOwner(String leagueOwner) {
        this.leagueOwner = leagueOwner;
    }

    public String getTeamOne() {
        return teamOne;
    }

    public void setTeamOne(String teamOne) {
        this.teamOne = teamOne;
    }

    public String getTeamTwo() {
        return teamTwo;
    }

    public void setTeamTwo(String teamTwo) {
        this.teamTwo = teamTwo;
    }

    public String getTeamOneScore() {
        return teamOneScore;
    }

    public void setTeamOneScore(String teamOneScore) {
        this.teamOneScore = teamOneScore;
    }

    public String getTeamTwoScore() {
        return teamTwoScore;
    }

    public void setTeamTwoScore(String teamTwoScore) {
        this.teamTwoScore = teamTwoScore;
    }

    public String getSeasonStarted() {
        return seasonStarted;
    }

    public void setSeasonStarted(String seasonStarted) {
        this.seasonStarted = seasonStarted;
    }
}

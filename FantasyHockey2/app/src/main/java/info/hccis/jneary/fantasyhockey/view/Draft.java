package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.Player;
import info.hccis.jneary.fantasyhockey.model.User;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;
import info.hccis.jneary.fantasyhockey.util.Utility;

public class Draft extends AppCompatActivity {

    private TextView startDraft;
    private Connect connect;
    private Player player;
    private ListView playerList;
    private Intent intent;
    private Button startDraftBtn;
    private Gson gson;
    private int numberOfTeams;
    private String draft;
    private ServerCallResults scr;
    private int playersTurn;
    private Map<String,Object> parsed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayout draftLayout = (LinearLayout) findViewById(R.id.layoutDraft);
        playersTurn = Integer.parseInt(getIntent().getExtras().get("playersTurn").toString());
        draft = getIntent().getExtras().get("draft").toString();
        numberOfTeams =Integer.parseInt(getIntent().getExtras().get("numberOfTeams").toString());
        startDraft = (TextView) findViewById(R.id.txtViewDraftStartDraft);
        playerList = (ListView) findViewById(R.id.listOfPlayers);
        startDraftBtn = (Button) findViewById(R.id.btnstartDraft);
        if (draft.equals("false") && numberOfTeams > 1 &&playersTurn==0) {
            startDraft.setText("Start Draft");

        } else if (draft.equals("false") && numberOfTeams %2!=0) {
            startDraft.setText("Not enough teams to start draft");
            startDraftBtn.setVisibility(View.INVISIBLE);


        } else {
            startDraft.setVisibility(View.INVISIBLE);
            startDraftBtn.setVisibility(View.INVISIBLE);
            draftLayout.setWeightSum(0);
            connect = new Connect("getPlayers.php");
            connect.execute("");
            gson = new Gson();

            player = new Player();


            try {

                //String jsonString=gson.toJson(connect.get());
                  parsed = gson.fromJson(connect.get(), Map.class);
                  player.parseMap(parsed);
                Log.d("eddd", "onCreate: players");
                //player = gson.fromJson(connect.get(), Player.class);
            } catch (InterruptedException | ExecutionException e) {

                e.printStackTrace();

            }catch (JsonSyntaxException e){
                Log.d("test", e.getMessage());
            }
            ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    player.returnPlayers()
            );
            playerList.setAdapter(listAdapter);
            playerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Player singlePlayer = player.returnSinglePlayer(position);
                    intent = new Intent(Draft.this, ViewSinglePlayerInformation.class);
                    intent.putExtra("player", singlePlayer);
                    startActivity(intent);


                }
            });


        }

    }

    public void viewPlayer(View view) {
        String[] params = new String[2];
        params[0] = "userName";
        params[1] = User.getUserName();
        connect = new Connect(params, "startDraft.php");
        connect.execute("");
        gson = new Gson();
        scr = new ServerCallResults();
        try {
            scr=gson.fromJson(connect.get(),ServerCallResults.class);



            if(scr.getStatus().equals("success")){
                params = new String[4];
                params[0] = "randomNumber";
                params[1] = String.valueOf(Utility.randomNumber(1,numberOfTeams));
                params[2] = "userName";
                params[3] = User.getUserName();
                connect = new Connect(params,"firstPick.php");
                connect.execute("");
                gson = new Gson();
                scr=gson.fromJson(connect.get(), ServerCallResults.class);
                if(!scr.getStatus().equals("fail")){
                    Toast.makeText(getApplicationContext(),
                            scr.getStatus()+" starts draft", Toast.LENGTH_LONG).show();
                }

                intent = new Intent(Draft.this,MainActivity.class);

                startActivity(intent);
            }
            else{
                Toast.makeText(getApplicationContext(),
                        "Draft could not be started", Toast.LENGTH_LONG).show();
                intent = new Intent(Draft.this,MainActivity.class);
                startActivity(intent);
            }
        } catch (InterruptedException | ExecutionException e) {

            e.printStackTrace();
        }

    }
}

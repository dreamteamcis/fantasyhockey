package info.hccis.jneary.fantasyhockey.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import info.hccis.jneary.fantasyhockey.Connection.Connect;
import info.hccis.jneary.fantasyhockey.R;
import info.hccis.jneary.fantasyhockey.model.User;
import info.hccis.jneary.fantasyhockey.util.Exceptions;
import info.hccis.jneary.fantasyhockey.util.ServerCallResults;
import info.hccis.jneary.fantasyhockey.util.Utility;

public class CreateAccount extends AppCompatActivity {
    private EditText userName;
    private EditText password;
    private EditText confirmPassword;
    private EditText email;
    private EditText teamName;
    private Button createAccountButton;
    private Connect connect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        createAccountButton = (Button) findViewById(R.id.createAccountButton);
        userName = (EditText) findViewById(R.id.createUserName);
        password = (EditText) findViewById(R.id.createAccountPassword);
        confirmPassword = (EditText) findViewById(R.id.createAccountConfirmPassword);
        email = (EditText) findViewById(R.id.createAccountEmail);
        teamName = (EditText) findViewById(R.id.createAccountTeamName);
    }
    public void clickCreateAccount(View view) {
        try {

            if(teamName.getText().toString().trim().isEmpty()){
                throw new Exceptions("Please fill out all fields");
            }
            String formattedTeam=Utility.formatString(teamName.getText().toString().trim());
            if(!password.getText().toString().equals(confirmPassword.getText().toString())){
                throw new Exceptions("Passwords don't match");
            }
            String formattedUsername = Utility.formatString(userName.getText().toString().trim());
            if (!Utility.verifyUserPassword(formattedUsername, password.getText().toString(), email.getText().toString())) {

                throw new Exceptions("Username, password, or email was invalid");

            }

            if(!Utility.sanitizeTeamName(teamName.getText().toString())){
                throw new Exceptions("Team name must be 9 characters or less.");
            }
            String[] params = new String[] {"userName", formattedUsername,"password", password.getText().toString(),
                    "email", email.getText().toString(), "teamName", formattedTeam};
            Connect connect = new Connect(params, "createAccount.php");
            connect.execute("");
            Gson gson = new Gson();
            try {
                ServerCallResults scr = gson.fromJson(connect.get(), ServerCallResults.class);

                if (scr.getStatus().equals("success")) {
                    Toast.makeText(getApplicationContext(),
                            "create make great success", Toast.LENGTH_LONG).show();
                    User.setUserName(userName.getText().toString());
                    Intent intent = new Intent(CreateAccount.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(),

                            "Could not add user", Toast.LENGTH_LONG).show();
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }





        }catch(Exceptions e){
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }
}

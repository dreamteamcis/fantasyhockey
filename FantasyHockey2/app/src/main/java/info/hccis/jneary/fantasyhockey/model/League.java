package info.hccis.jneary.fantasyhockey.model;

import java.util.List;

/**
 * Created by Jeff on 12/02/2016.
 */
public class League {

    private List<League> leagues;
    private int numberOfTeams;
    private boolean draftStarted;
    private String leagueName;
    private int leagueId;

    public League(){

    }
    public League(int numberOfTeams, boolean draftStarted, String leagueName, int leagueId) {
        this.numberOfTeams = numberOfTeams;
        this.draftStarted = draftStarted;
        this.leagueName = leagueName;
        this.leagueId = leagueId;
    }


    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

    public int getNumberOfTeams() {
        return numberOfTeams;
    }
    public String[] returnTitles(){
        String[]titles = new String[leagues.size()];
        for(int i=0;i<leagues.size();i++){
            titles[i]=leagues.get(i).getLeagueName();
        }
        return titles;
    }
    public void setNumberOfTeams(int numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    public boolean isDraftStarted() {
        return draftStarted;
    }

    public void setDraftStarted(boolean draftStarted) {
        this.draftStarted = draftStarted;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public int getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(int leagueId) {
        this.leagueId = leagueId;
    }

    @Override
    public String toString() {
        return "League{" +
                "leagues=" + leagues +
                ", numberOfTeams=" + numberOfTeams +
                ", draftStarted=" + draftStarted +
                ", leagueName='" + leagueName + '\'' +
                ", leagueId=" + leagueId +
                '}';
    }
}

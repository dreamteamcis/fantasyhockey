
<?php
/**
 * Page Name: readPlayers.php
 * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson
 * Purpose: sets up all players int he db.
 *
 * Date: 09/02/2016
 * Time: 11:59 AM
 */

require('php-excel-reader/excel_reader2.php');

require('SpreadsheetReader.php');
$players = array();
$formattedPlayers = array();
//connects to spreadsheet
$Reader = new SpreadsheetReader('Hockeystats.xlsx');


/**
 * Created by PhpStorm.
 * User: Jay
 * Date: 2016-02-12
 * Time: 5:53 PM
 */
$league = 48;
$teams = array();
$selectTeams= "SELECT `team_id` FROM `Teams` WHERE league_id =".$league;
$db = new mysqli('localhost', 'jneary_admin', 'admin', 'jneary_fantasyhockey', 2082);




$teamOne = 0;
$teamTwo = 0;
$matchId = 0;
$teamOnePoints = 0;
$teamTwoPoints = 0;
$teamPointsArray = array();
$teamPointsArray2 = array();
$finalPointsArray = array();
$selectMatches = "SELECT * FROM `Matches`";
$results = $db->query($selectMatches);
$numberOfResults = $results->num_rows;
$matcharray = array();
$team1ArrayPoints = array();
$team2ArrayPoints = array();

for($i=0;$i<$numberOfResults; $i++){
    $row = $results ->fetch_assoc();
    $teamOne = $row['team_one'];
    $teamTwo = $row['team_two'];
    $matchId = $row['match_id'];
    $teamOnePoints = $row['team_one_points'];
    $teamTwoPoints = $row['team_two_points'];
    array_push($matcharray, $matchId);
    array_push($team1ArrayPoints, $teamOnePoints);
    array_push($team2ArrayPoints, $teamTwoPoints);
    for($j=0;$j<2; $j++){
        $currentTeam = 0;
        if($j==0){
            $currentTeam = $teamOne;

        }else{
            $currentTeam = $teamTwo;
        }
        $sql2 = "SELECT * FROM `Players` WHERE team_id =".$currentTeam;
        $results2 = $db->query($sql2);
        $numberOfResults2 = $results2->num_rows;
        $teamOnePoints = 0;
        $teamTwoPoints = 0;

        for($k=0;$k<$numberOfResults2;$k++){
            $row = $results2->fetch_assoc();
            $playerId = $row['player_id'];
            $points = $row['points'];
            $gamesPlayed = $row['games_played'];

            if($currentTeam == $teamOne){

                $teamOnePoints += $points;
                $teamOneGames += $games;
                if($k == $numberOfResults2 -1){
                    array_push($teamPointsArray, $teamOnePoints);

                }
            }else{
                $teamTwoPoints += $points;
                if($k == $numberOfResults2 -1){
                    array_push($teamPointsArray, $teamTwoPoints);
                }
            }


        }
    }

}
print_r($teamPointsArray);



foreach ($Reader as $Row)
{

    array_push($players,$Row);


}

    for ($i = 0; $i < sizeof($players); $i++) {


        //formats return values so only specific information is returned.
        if (sizeof($players[$i]) == 27) {
            if ($players[$i][26] != "FO%" && $players[$i][1] != "") {
                $name = $players[$i][1];
                $gamesPlayed = $players[$i][5];
                $goals = $players[$i][6];
                $points = $players[$i][8];
                $sql = "UPDATE Players SET goals = " . $goals . ", points = " . $points . ",games_played = " . $gamesPlayed . " WHERE player_name = '" . $name . "'";

                if (mysqli_query($db, $sql)) {

                } else {
                    "added " . $name . " failed.";
                }


            }
        }

    }
$results = $db->query($selectMatches);
for($i=0;$i<$numberOfResults; $i++){
    $row = $results ->fetch_assoc();
    $teamOne = $row['team_one'];
    $teamTwo = $row['team_two'];
    $matchId = $row['match_id'];
    $teamOnePoints = $row['team_one_points'];
    $teamTwoPoints = $row['team_two_points'];

    for($j=0;$j<2; $j++){
        $currentTeam = 0;
        if($j==0){
            $currentTeam = $teamOne;

        }else{
            $currentTeam = $teamTwo;
        }
        $sql2 = "SELECT * FROM `Players` WHERE team_id =".$currentTeam;
        $results2 = $db->query($sql2);
        $numberOfResults2 = $results2->num_rows;
        $teamOnePoints = 0;
        $teamTwoPoints = 0;
        for($k=0;$k<$numberOfResults2;$k++){
            $row = $results2->fetch_assoc();
            $playerId = $row['player_id'];
            $points = $row['points'];
            $gamesPlayed = $row['games_played'];
            if($currentTeam == $teamOne){

                $teamOnePoints += $points;
                if($k == $numberOfResults2 -1){
                    array_push($teamPointsArray2, $teamOnePoints);
                }
            }else{
                $teamTwoPoints += $points;
                if($k == $numberOfResults2 -1){
                    array_push($teamPointsArray2, $teamTwoPoints);
                }
            }


        }
    }
}
$superFinalPointsArray = array();
for($i=0;$i<sizeof($teamPointsArray);$i++) {
    $finalPointsArray[$i] = $teamPointsArray2[$i] - $teamPointsArray[$i];
    if ($i % 2 == 0) {
        $team1ArrayPoints[$i] = $team1ArrayPoints[$i] + $finalPointsArray[$i];
    } else {
        $team2ArrayPoints[$i] = $team2ArrayPoints[$i] + $finalPointsArray[$i];
    }


}
print_r($finalPointsArray);
$counter = 0;
for($i=0;$i<sizeof($matcharray);$i++){
    $sql3 = "UPDATE Matches SET team_one_points =".$team1ArrayPoints[$i].", team_two_points =".$team2ArrayPoints[$i]." WHERE match_id = ".$matcharray[$i];
    if(!$db->query($sql3)){
        echo "fail";
    }
    $counter+=2;
}
print_r($finalPointsArray);
$results->free();
$results2->free();
$db->close();



<?php
/**
 * Created by PhpStorm.
 * User: Jay
 * Date: 2016-02-18
 * Time: 12:14 AM
 */
$leagueId = $_GET['leagueId'];
$standings = array();
$teamName = "";
$wins = 0;
$sql = "SELECT * FROM Teams WHERE league_id =".$leagueId." ORDER BY team_wins";
$db = new mysqli('localhost', 'jneary_admin', 'admin', 'jneary_fantasyhockey', 2082);
$results = $db->query($sql);
$numberOfRows = $results->num_rows;
for($i=0; $i<$numberOfRows; $i++){
    $row = $results->fetch_assoc();
    $teamName = $row['team_name'];
    $wins = $row['team_wins'];
    $standings[$i] = array('teamName'=>$teamName, 'wins'=>$wins);
}
$results->free();
$db->close();
echo "{"."\"standings\": ". json_encode($standings). "\n}";


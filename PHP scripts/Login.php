<?php
/**
 * Page Name: Login.php
 * Created by Jeff N,Jeff G,Jason N, Chelsea Ferguson
 * Purpose: Script takes 2 get parameters, retrieves username and password information
 * from the users table. If username and password are correct a success json object is passed, on failure a failure
 * object is passed back.
 * Date: 24/01/2016
 * Time: 11:59 AM
 */

$login = false;
$loginMessage=array('status'=>"fail");
if(isset($_GET['userName'])&&isset($_GET['password'])) {
    $user = trim($_GET['userName']);
    $pass = trim(md5($_GET['password']));


    @ $db = new mysqli('localhost', 'jneary_admin', 'admin', 'jneary_fantasyhockey', 2082);
    if (mysqli_connect_errno()) {
        echo "fail";

    }
    $sql = "SELECT * FROM Users";
    $result = $db->query($sql);
    $numberOfRows = $result->num_rows;
    for ($i = 0; $i < $numberOfRows; $i++) {
        $row = $result->fetch_assoc();
        $userName = $row['user_name'];
        $password = $row['user_password'];
        if($userName==htmlspecialchars($user)&&$password==htmlspecialchars($pass)){
			$login = true;
            $loginMessage=array('status'=>"success");
            echo json_encode($loginMessage);
        }
        



    }
	if($login==false){
		echo json_encode($loginMessage);
	}
    $result->free();
    $db->close();

}

?>